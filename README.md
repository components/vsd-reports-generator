# Value Streams Management Scheduled Reports

The Value Streams Dashboard Scheduled Reports tool is a [CI/CD component](https://docs.gitlab.com/ee/ci/components/) which allows you to periodically schedule a report with the most recent metrics from the [Value Streams Dashboard](https://docs.gitlab.com/ee/user/analytics/value_streams_dashboard.html) feature.

The tool collects metrics from projects or groups via the public GitLab GraphQL API, then builds a report using the GitLab Flavored [Markdown format](https://docs.gitlab.com/ee/user/markdown.html). As the last step, an issue is opened in the designated project, with markdown comparison metrics table, like in [this example](#example-for-monthly-executive-value-streams-report).


## How to use the CI/CD component

### Requirements

- A GitLab project that contains the Value Streams Dashboard configuration YAML file. See an [example file](https://gitlab.com/components/vsd-reports-generator/-/blob/main/.gitlab/analytics/dashboards/value_streams/value_streams.yaml) and learn more about [customization options](https://docs.gitlab.com/ee/user/analytics/value_streams_dashboard.html#customize-the-dashboard-panels).
- An Ultimate license for the group or project. The tool works without a license, but most metrics are not available.
- A GitLab project that has a configured `.gitlab-ci.yml` file to run the CI/CD component.
- An API token that can read the configured projects and groups, and open an issue in the designated project.
  - We recommentd setting up a [service account](https://docs.gitlab.com/ee/user/profile/service_accounts.html) for this report.
  - Alternatively access tokens can be used:
    - [Personal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html)
    - [Group access token](https://docs.gitlab.com/ee/user/group/settings/group_access_tokens.html)

### Add the component

In the `.gitlab-ci.yml` file, add the following snippet:

```yaml
include:
  - component: gitlab.com/components/vsd-reports-generator/vsd-reports-generator@~latest
    inputs:
      vsd_project_config_url: "https://gitlab.com/my-group/vsd-config-project"
      access_token: $GITLAB_ACCESS_TOKEN
      open_issue_in_project_path: "my-group/my-subgroup/project-for-reports"
      open_issue_assignee_list: "\"@my_user @another_user\""
      open_issue_label_list: "~\"reports::vsd\""
```

### Configure the access token

To securely store the access token for the API calls, you must configure it as a [CI/CD variable](https://docs.gitlab.com/ee/ci/variables/#for-a-project).

1. Open the GitLab project containing the `.gitlab-ci.yml` file.
1. Go to `Settings > CI/CD`.
1. Open the `Variables` section.
1. Add a new variable (make sure masking is enabled) called `GITLAB_ACCESS_TOKEN`.
1. Add the access token value, then select `Add variable`.

### Test the pipeline

1. Open the GitLab project containing the `.gitlab-ci.yml` file.
1. Go to `Build > Pipelines`.
1. Select `Run pipeline`.
1. Inspect the pipeline output and verify that the issue with the metrics is opened correctly.

### Set up a pipeline schedule

1. Open the GitLab project containing the `.gitlab-ci.yml` file.
1. Go to `Build > Pipeline schedules`.
1. Select `Create a new pipeline schedule`.
1. Configure the interval pattern.
1. Select `Create pipeline schedule`.

#### Conditional execution

When specifying the component invocation in your `.gitlab-ci.yml` file you may want to run the
component in specific cases:

- When the pipeline is invoked in a pipeline schedule.
- When a certain variable is present.

```yaml
include:
  - component: gitlab.com/components/vsd-reports-generator/vsd-reports-generator@~latest
    inputs:
      # inputs here
    rules:
      - if: '$RUN_VSD_REPORT == "true"' # the component only runs when the RUN_VSD_REPORT variable is set 
```

```yaml
include:
  - component: gitlab.com/components/vsd-reports-generator/vsd-reports-generator@~latest
    inputs:
      # inputs here
    rules:
      - if: '$CI_PIPELINE_SOURCE == "schedule"' # the component only runs when it's invoked via pipeline schedule
```

### Configuration options

The following YAML configuration options are available:

|Variable name|Description|Required|
|-|-|-|
|`vsd_config_project_url`|The full URL to the GitLab project where the VSD YAML file is located|Yes|
|`access_token`|API access token that covers all project or group paths defined in the VSD YAML file|Yes|
|`dashboard_yaml_path`|Use a custom path for the VSD YAML file within the project repo. Defaults to: `.gitlab/analytics/dashboards/value_streams/value_streams.yaml`|No|
|`open_issue_in_project_path`|Project path where a new confidential issue is opened after the report generation|No|
|`open_issue_assignee_list`|List of usernames the opened issue is assigned to. Example: `@username1 @username2`|No|
|`open_issue_label_list`|List of labels for the opened issue. Example: `~"backend" ~"workflow::review"`|No|
|`open_issue_title_prefix`|Custom text for the issue title. Defaults to the configured dashboard title.|No|

## Usage (invoking the tool directly, only for development)

The tool can be configured using the following environment variables:

|Variable name|Description|Required|
|-|-|-|
|`VSD_CONFIG_PROJECT_URL`|The full URL to the GitLab project where the Value Streams Dashboard YAML file is located|Yes|
|`ACCESS_TOKEN`|API access token that covers all project or group paths defined in the VSD YAML file|Yes|
|`DASHBOARD_YAML_PATH`|Use a custom path for the VSD YAML file within the project repo. Defaults to: `.gitlab/analytics/dashboards/value_streams/value_streams.yaml`|No|
|`OPEN_ISSUE_IN_PROJECT_PATH`|Project path where a new confidential issue is opened after the report generation|No|
|`OPEN_ISSUE_ASSIGNEE_LIST`|List of usernames where the opened issue is assigned to. Example: `@username1 @username2`|No|
|`OPEN_ISSUE_LABEL_LIST`|List of labels for the opened issue. Example: `~"backend" ~"workflow::review"`|No|
|`OPEN_ISSUE_TITLE_PREFIX`|Custom text for the issue title. Defaults to the configured dashboard title|No|

To invoke the tool, execute the following command:

```bash
VSD_CONFIG_PROJECT_URL=https://gitlab.com/group1/project1 ACCESS_TOKEN=YOUR_TOKEN bin/report

cat report.md
```

Configure the tool to open an issue:

```bash
OPEN_ISSUE_ASSIGNEE_LIST='@user1 @root'
OPEN_ISSUE_LABEL_LIST='~"backend" ~"my::label"'
OPEN_ISSUE_IN_PROJECT_PATH=group/project
VSD_CONFIG_PROJECT_URL=http://localhost:3000/group/project
ACCESS_TOKEN=YOUR_TOKEN
bin/report
```

### Example for Monthly Executive Value Streams Report 

|[Gitlab.org group](https://gitlab.com/groups/gitlab-org/-/analytics/dashboards/value_streams_dashboard)|Month to date: 2024-03-01 - 2024-03-25|2024-02-01 - 2024-02-29|2024-01-01 - 2024-01-31|
|-|-|-|-|
|Issues created|12880 {+ +15.2% +}|11183 {- -13.4% -}|12909 {+ +36.5% +}|
|Deployment frequency|78.8 d {- -51.4% -}|162.0 d {+ +4.7% +}|154.7 d {+ +6.7% +}|
|Lead time for changes|0.2 d |0.2 d |0.2 d|
|Time to restore service|14.5 d {+ +663.2% +}|1.9 d {+ +216.7% +}|0.6 d {- -76.0% -}|
|Change failure rate|0.0%|0.0%|0.0%|
|Lead time|30.5 d |30.5 d {- -0.7% -}|30.7 d {+ +1.7% +}|
|Cycle time|3.2 d {- -31.9% -}|4.7 d {- -13.0% -}|5.4 d {+ +86.2% +}|
|Code Suggestions usage| 39.1%  {+ +10.2% +}|42.4% {+ +23.5% +}|53.3% {+ +86.2% +}|
