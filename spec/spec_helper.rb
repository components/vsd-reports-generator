# frozen_string_literal: true

require 'webmock/rspec'
require 'vcr'
require 'stub_env'
require_relative '../lib/value_stream_dashboard'
require 'active_support/testing/time_helpers'

ENV['VSD_CONFIG_PROJECT_URL'] = 'https://gdk.localhost:3443/gitlab-org/gitlab-shell'
ENV['ACCESS_TOKEN'] ||= 'SOME_TOKEN'

VCR.configure do |config|
  config.cassette_library_dir = "spec/fixtures/vcr_cassettes"
  config.hook_into :webmock
  config.define_cassette_placeholder("<ACCESS_TOKEN>", ENV.fetch("ACCESS_TOKEN", nil))

  operation_definition_regex = /OperationDefinition_(\d+)/
  config.register_request_matcher :normalized_body do |request1, request2|
    body1 = request1.body
    body2 = request2.body
    body1.gsub(operation_definition_regex, 'id') == body2.gsub(operation_definition_regex, 'id')
  end
  config.default_cassette_options = {
    match_requests_on: %i[uri method normalized_body]
  }
end

RSpec.configure do |config|
  config.include StubEnv::Helpers
  config.include ActiveSupport::Testing::TimeHelpers
  config.order = 'random'

  config.around(:example, :vcr) do |example|
    VCR.use_cassette('all_requests', record: :none) do
      example.run
    end
  end
end
