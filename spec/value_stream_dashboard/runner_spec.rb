# frozen_string_literal: true

require 'spec_helper'

RSpec.describe ValueStreamDashboard::Runner do
  let(:runner) { described_class.new }

  before { travel_to(DateTime.new(2023, 8, 7, 12, 5)) }
  # this is UTC time, so local time might be ahead or behind.

  it 'generates a report new runner', :vcr do
    runner.run

    expect(File).to exist('report.md')
  end

  context 'when specifying the issue arguments', :vcr do
    before do
      stub_env('OPEN_ISSUE_IN_PROJECT_PATH', 'group/project')
    end

    it 'invokes the IssueCreator' do
      allow(runner.config[:http_client]).to receive(:create_issue)

      runner.run

      expect(runner.config[:http_client]).to have_received(:create_issue).with(
        title: "Custom Dashboard title — #{Time.current.to_date}",
        description: include("Monthly Executive Value Streams Report for August")
      )
    end
  end
end
