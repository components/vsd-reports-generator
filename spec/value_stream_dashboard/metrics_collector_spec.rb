# frozen_string_literal: true

require 'spec_helper'

RSpec.describe ValueStreamDashboard::MetricsCollector do
  let(:panel_config) do
    {
      'title' => 'Panel Title',
      'data' => { 'namespace' => 'group/subgroup' }
    }
  end

  let(:http_client) { instance_double(ValueStreamDashboard::API::Client) }
  let(:collector) { described_class.new(panel: panel_config, http_client:) }

  before do
    allow(Time).to receive(:current).and_return(instance_double(DateTime, utc: DateTime.new(2023, 8, 7, 12, 5).utc))
  end

  describe '#headers' do
    subject(:headers) { collector.headers }

    it 'correctly formats the header' do
      allow(http_client).to receive(:load_project).and_return({ 'id' => 5, 'webUrl' => 'https://gitlab.com/gitlab-org/gitlab' })

      expect(headers).to eq([
                              '[Panel Title](https://gitlab.com/gitlab-org/gitlab/-/analytics/dashboards/value_streams_dashboard)',
                              [DateTime.new(2023, 8, 1).utc.beginning_of_day, Time.current.utc],
                              [DateTime.new(2023, 7, 1).utc.beginning_of_day, DateTime.new(2023, 7, 31).utc.end_of_day],
                              [DateTime.new(2023, 6, 1).utc.beginning_of_day, DateTime.new(2023, 6, 30).utc.end_of_day],
                              [DateTime.new(2023, 5, 1).utc.beginning_of_day, DateTime.new(2023, 5, 31).utc.end_of_day]
                            ])
    end

    context 'when group is given' do
      it 'correctly formats the header' do
        allow(http_client).to receive(:load_project).and_return(nil)
        allow(http_client).to receive(:load_group).and_return({ 'id' => 1, 'webUrl' => 'https://gitlab.com/groups/gitlab-org' })

        expect(headers).to eq([
                                '[Panel Title](https://gitlab.com/groups/gitlab-org/-/analytics/dashboards/value_streams_dashboard)',
                                [DateTime.new(2023, 8, 1).utc.beginning_of_day, Time.current.utc],
                                [DateTime.new(2023, 7, 1).utc.beginning_of_day, DateTime.new(2023, 7, 31).utc.end_of_day],
                                [DateTime.new(2023, 6, 1).utc.beginning_of_day, DateTime.new(2023, 6, 30).utc.end_of_day],
                                [DateTime.new(2023, 5, 1).utc.beginning_of_day, DateTime.new(2023, 5, 31).utc.end_of_day]
                              ])
      end
    end
  end

  describe '#filters' do
    context 'when no filter is used' do
      it 'applies no filter' do
        allow(http_client).to receive(:load_project).and_return(nil)
        expect(http_client).to receive(:new_issues).with(a_hash_including(project: false)).at_least(:once)
        # do not expect to receive any filters
        expect(http_client).not_to receive(:new_issues).with(a_hash_including(labelNames: anything))
        expect(http_client).to receive(:deployment_frequency).with(a_hash_including(project: false)).at_least(:once)
        expect(http_client).to receive(:lead_time_for_changes).with(a_hash_including(project: false)).at_least(:once)
        expect(http_client).to receive(:time_to_restore_service).with(a_hash_including(project: false)).at_least(:once)
        expect(http_client).to receive(:change_failure_rate).with(a_hash_including(project: false)).at_least(:once)
        expect(http_client).to receive(:lead_time).with(a_hash_including(project: false)).at_least(:once)
        expect(http_client).not_to receive(:lead_time).with(a_hash_including(labelNames: anything))
        expect(http_client).to receive(:cycle_time).with(a_hash_including(project: false)).at_least(:once)
        expect(http_client).not_to receive(:cycle_time).with(a_hash_including(labelNames: anything))
        expect(http_client).to receive(:code_suggestion_usage_rate).with(a_hash_including(project: false)).at_least(:once)

        collector.data
      end
    end

    context 'when label filter is used' do
      let(:panel_config) do
        {
          'title' => 'Panel Title',
          'data' => { 'namespace' => 'group/subgroup' },
          'queryOverrides' => { 'filters' => { 'labels' => 'in_dev', 'nonexistent' => 'nothing' } }
        }
      end

      before do
        allow(http_client).to receive(:load_group).and_return({ 'id' => 1, 'webUrl' => 'https://gitlab.com/groups/gitlab-org' })
      end

      it 'applies the filter only to fields that support them' do
        allow(http_client).to receive(:load_project).and_return(nil)
        expect(http_client).to receive(:new_issues).with(a_hash_including(project: false, labelNames: 'in_dev')).at_least(:once)
        expect(http_client).to receive(:deployment_frequency).with(a_hash_including(project: false)).at_least(:once)
        expect(http_client).not_to receive(:deployment_frequency).with(a_hash_including(labelNames: anything))
        expect(http_client).to receive(:lead_time_for_changes).with(a_hash_including(project: false)).at_least(:once)
        expect(http_client).not_to receive(:lead_time_for_changes).with(a_hash_including(labelNames: anything))
        expect(http_client).to receive(:time_to_restore_service).with(a_hash_including(project: false)).at_least(:once)
        expect(http_client).not_to receive(:time_to_restore_service).with(a_hash_including(labelNames: anything))
        expect(http_client).to receive(:change_failure_rate).with(a_hash_including(project: false)).at_least(:once)
        expect(http_client).not_to receive(:change_failure_rate).with(a_hash_including(labelNames: anything))
        expect(http_client).to receive(:lead_time).with(a_hash_including(project: false, labelNames: 'in_dev')).at_least(:once)
        expect(http_client).to receive(:cycle_time).with(a_hash_including(project: false, labelNames: 'in_dev')).at_least(:once)
        expect(http_client).to receive(:code_suggestion_usage_rate).with(a_hash_including(project: false)).at_least(:once)
        expect(http_client).not_to receive(:code_suggestion_usage_rate).with(a_hash_including(labelNames: anything))

        collector.data
      end
    end
  end

  describe '#data' do
    subject(:data) { collector.data }

    it 'correctly formats the data' do
      allow(http_client).to receive(:load_project).and_return(nil)
      allow(http_client).to receive(:load_group).and_return({ 'id' => 1, 'webUrl' => 'https://gitlab.com/groups/gitlab-org' })
      allow(http_client).to receive(:new_issues).with(a_hash_including(project: false)).and_return(1, 2, 3, 3)
      allow(http_client).to receive(:deployment_frequency).with(a_hash_including(project: false)).and_return(1, 2, 3, 7)
      allow(http_client).to receive(:lead_time_for_changes).with(a_hash_including(project: false)).and_return(2, 3, 4, 5)
      allow(http_client).to receive(:time_to_restore_service).with(a_hash_including(project: false)).and_return(5, 6, 7, 8)
      allow(http_client).to receive(:change_failure_rate).with(a_hash_including(project: false)).and_return(6, 7, 8, 9)
      allow(http_client).to receive(:lead_time).with(a_hash_including(project: false)).and_return(2.1, 3.2, 4.3, 5.4)
      allow(http_client).to receive(:cycle_time).with(a_hash_including(project: false)).and_return(2.5, 3.5, 4.5, 5.5)
      allow(http_client).to receive(:code_suggestion_usage_rate).with(a_hash_including(project: false)).and_return(15.5, 30.2, 45.5, 44.1)

      raw_values = data.map do |row|
        metric, *rest = row
        [metric, *rest.map(&:raw_value)]
      end

      expect(raw_values).to eq([
                                 [I18n.t('metrics.new_issues'), 1, 2, 3, 3],
                                 [I18n.t('metrics.deployment_frequency'), 1, 2, 3, 7],
                                 [I18n.t('metrics.lead_time_for_changes'), 2, 3, 4, 5],
                                 [I18n.t('metrics.time_to_restore_service'), 5, 6, 7, 8],
                                 [I18n.t('metrics.change_failure_rate'), 6, 7, 8, 9],
                                 [I18n.t('metrics.lead_time'), 2.1, 3.2, 4.3, 5.4],
                                 [I18n.t('metrics.cycle_time'), 2.5, 3.5, 4.5, 5.5],
                                 [I18n.t('metrics.code_suggestion_usage_rate'), 15.5, 30.2, 45.5, 44.1]
                               ])
    end
  end
end
