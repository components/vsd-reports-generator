# frozen_string_literal: true

require 'spec_helper'

RSpec.describe ValueStreamDashboard::IssueCreator, :vcr do
  let(:config) { ValueStreamDashboard::Config.build }
  let(:table_content) { 'content' }
  let(:issue_creator) { described_class.new(config:, table_content:) }

  let(:invoke) do
    allow(config[:http_client]).to receive(:create_issue)

    issue_creator.call
  end

  it 'creates the issue on the target project' do
    invoke

    expect(config[:http_client]).to have_received(:create_issue).with(
      title: "Custom Dashboard title — #{Time.current.to_date}",
      description: include(table_content)
    )
  end

  describe 'legend text for no data' do
    context 'when no data emoji is not in the content' do
      it 'does not add legend' do
        invoke

        expect(config[:http_client]).to have_received(:create_issue).with(
          title: "Custom Dashboard title — #{Time.current.to_date}",
          description: satisfy { |desc| !desc.include?(I18n.t('issue.no_data_legend_text')) }
        )
      end
    end

    context 'when no data emoji is in the content' do
      let(:table_content) { '|❌|1|' }

      it 'adds the legend' do
        invoke

        expect(config[:http_client]).to have_received(:create_issue).with(
          title: "Custom Dashboard title — #{Time.current.to_date}",
          description: include(I18n.t('issue.no_data_legend_text'))
        )
      end
    end
  end

  context 'when labels are present' do
    it 'adds the /labels slash command' do
      stub_env('OPEN_ISSUE_LABEL_LIST', '~label1 ~"aa:bb"')

      invoke

      expect(config[:http_client]).to have_received(:create_issue).with(
        title: "Custom Dashboard title — #{Time.current.to_date}",
        description: include('/labels ~label1 ~"aa:bb"')
      )
    end
  end

  context 'when assignees are present' do
    it 'adds the /assignees slash command' do
      stub_env('OPEN_ISSUE_ASSIGNEE_LIST', '@user1 @user2')

      invoke

      expect(config[:http_client]).to have_received(:create_issue).with(
        title: "Custom Dashboard title — #{Time.current.to_date}",
        description: include('/assign @user1 @user2')
      )
    end
  end

  context 'when customizing the issue title' do
    it 'uses the given issue title' do
      stub_env('OPEN_ISSUE_TITLE_PREFIX', 'my issue')

      invoke

      expect(config[:http_client]).to have_received(:create_issue).with(
        title: "my issue — #{Time.current.to_date}",
        description: include(table_content)
      )
    end
  end
end
