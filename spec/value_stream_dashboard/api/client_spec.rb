# frozen_string_literal: true

require 'spec_helper'

RSpec.describe ValueStreamDashboard::API::Client do
  subject(:client) { described_class.build(config) }

  let(:config) { ValueStreamDashboard::Config.build }

  describe 'DORA metrics' do
    let(:args) { { project: 'path', from: Date.parse('2022-05-01'), to: Date.parse('2024-05-10') } }

    %i[deployment_frequency change_failure_rate lead_time_for_changes time_to_restore_service].each do |method|
      describe "##{method}" do
        context 'when not authorized' do
          it 'returns nil' do
            allow(client.client).to receive_message_chain(:query, :to_h).and_return({})

            value = client.public_send(method, **args)
            expect(value).to be_nil
          end
        end
      end
    end
  end

  describe '#read_config_file' do
    it 'raises error when the project cannot be found' do
      allow(client.client).to receive_message_chain(:query, :data, :project).and_return(nil)
      expect { client.read_config_file }.to raise_error(/Couldn't access project/)
    end

    it 'raises error when the yml_file is missing' do
      allow(client.client).to receive_message_chain(:query, :data, :project, :repository, :blobs, :nodes).and_return([])

      expect { client.read_config_file }.to raise_error(/Couldn't access the default config file/)
    end

    context 'when custom YML path is given' do
      it 'raises error when the yml_file is missing' do
        stub_env('DASHBOARD_YAML_PATH', 'some path')

        allow(client.client).to receive_message_chain(:query, :data, :project, :repository, :blobs, :nodes).and_return([])

        expect { client.read_config_file }.to raise_error(/Couldn't access the provided config file/)
      end
    end
  end
end
