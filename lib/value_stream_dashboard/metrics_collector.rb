# frozen_string_literal: true

module ValueStreamDashboard
  class MetricsCollector
    GITLAB_DASHBOARD_URL_SUFFIX = '/-/analytics/dashboards/value_streams_dashboard'

    METRICS_TO_COLLECT = {
      new_issues: {
        available_filters: ['labels' => 'labelNames']
      }.freeze,
      deployment_frequency: {
        format: 'days'
      }.freeze,
      lead_time_for_changes: {
        format: 'days'
      }.freeze,
      time_to_restore_service: {
        format: 'days'
      }.freeze,
      change_failure_rate: {
        format: 'percent'
      }.freeze,
      lead_time: {
        format: 'days',
        available_filters: ['labels' => 'labelNames']
      }.freeze,
      cycle_time: {
        format: 'days',
        available_filters: ['labels' => 'labelNames']
      }.freeze,
      code_suggestion_usage_rate: {
        format: 'percent'
      }.freeze
    }.freeze

    PAST_MONTHS = 2

    def initialize(panel:, http_client:)
      @panel = panel
      @http_client = http_client
    end

    def headers
      [formatted_title] + date_headers
    end

    def data
      @data ||= METRICS_TO_COLLECT.map do |metric_name, config|
        columns = [I18n.t("metrics.#{metric_name}")]

        date_headers.each do |start_date, end_date|
          args = {
            fullPath: full_path,
            from: start_date,
            to: end_date,
            project: project.present?
          }.merge(map_filters(config[:available_filters], filters))

          # pp metric_name
          # pp args
          raw_value = @http_client.public_send(
            metric_name,
            **args
          )
          columns << Column.new(raw_value:, config:)
        end

        columns
      end
    end

    private

    def full_path
      @panel.dig('queryOverrides', 'namespace') || @panel.dig('data', 'namespace')
    end

    # it's possible that the given filter key from the dashboard yml is different from the GraphQL API expects
    # Given available_filters [{'labels' => 'labelNames'}]
    # and dashboard yml filters {'labels' => ['in_dev', 'in_review']}
    # should return { 'labelNames' => ['in_dev', 'in_review']
    def map_filters(available_filters, given_filters)
      return {} unless available_filters.present? && given_filters.present?

      {}.tap do |arr|
        available_filters.each do |filter|
          filter.each do |k, v|
            arr[v.to_sym] = given_filters[k]
          end
        end
      end
    end

    def filters
      @panel.dig('queryOverrides', 'filters') || @panel.dig('data', 'filters') || {}
    end

    def project
      return @project if defined?(@project)

      @project = @http_client.load_project(full_path)
    end

    def group
      return @group if defined?(@group)

      @group = @http_client.load_group(full_path)
    end

    def formatted_title
      title = @panel['title'] || full_path
      web_url = project.present? ? project['webUrl'] : group['webUrl']

      url = web_url + GITLAB_DASHBOARD_URL_SUFFIX

      "[#{title}](#{url})"
    end

    def date_headers
      current_date = Time.current.utc

      columns = [[current_date.beginning_of_month, current_date]]

      # +1 because we want to calculate % change using the previous month
      (PAST_MONTHS + 1).times do
        current_date = current_date.prev_month
        columns << [current_date.beginning_of_month.beginning_of_day, current_date.end_of_month.end_of_day]
      end

      columns
    end
  end
end
