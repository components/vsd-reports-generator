# frozen_string_literal: true

module ValueStreamDashboard
  class IssueCreator
    def initialize(config:, table_content:)
      @config = config
      @table_content = table_content
    end

    def call
      config[:http_client].create_issue(title: build_title, description: build_description)
    end

    private

    attr_reader :config, :table_content

    def date
      @date ||= Time.current.to_date
    end

    def template_url
      File.join(
        config[:project_url],
        '-',
        'blob',
        'HEAD',
        config[:yml_path]
      )
    end

    def build_title
      prefix = config[:open_issue_title_prefix] || config.dashboard_config['title']
      "#{prefix} — #{date}"
    end

    def build_description
      month = I18n.t('month_names')[date.month - 1]

      [
        I18n.t('issue.title', date:, month:),
        '',
        I18n.t('issue.content', month:, template_url:),
        (['', I18n.t('issue.no_data_legend_text')] if missing_data?),
        '',
        table_content,
        '',
        *slash_commands
      ].flatten.compact.join("\n")
    end

    def slash_commands
      commands = []
      commands << "/labels #{config[:open_issue_label_list]}" if config[:open_issue_label_list]
      commands << "/assign #{config[:open_issue_assignee_list]}" if config[:open_issue_assignee_list]

      commands
    end

    def missing_data?
      table_content.include?(ValueStreamDashboard::Column::NO_DATA_EMOJI)
    end
  end
end
